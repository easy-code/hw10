let tasks = ajax.send({
    method: 'GET',
    url: 'https://jsonplaceholder.typicode.com/todos',
    success: function (res) {
        tasks = JSON.parse(res);
        console.log(tasks);
        generateList(tasks);
    },
    error: function (err) {
        showMessage(warningAlert);
        console.log(err);
    }
}) || [];


let ul = document.querySelector('.list-group'),
    form = document.forms['addTodoItem'],
    inputText = form.elements['todoText'],
    successAlert = document.querySelector('.alert-success'),
    removeAlert = document.querySelector('.alert-danger'),
    infoAlert = document.querySelector('.alert-info'),
    updateAlert = document.querySelector('.alert-secondary'),
    warningAlert = document.querySelector('.alert-warning');

function getTemplate(object) {
    const template = `
        <li data-id=${object.id} class="${object.completed ? "bg-success" : ""} list-group-item d-flex align-items-center" >
            <span class="task-name">${object.title}</span>
            <span class="management ml-auto">  
                <i class="fas fa-edit edit-itm edit-item"></i>
                <i class="fas ${object.completed ? "fa-check" : "fa-times"} ml-2 status"></i>
                <i class="fas fa-trash ml-2 delete-item"></i>
            </span>
        </li>
    `;

    return template
}

function generateList(tasksArray) {
    clearList();
    checkEmpty(tasks);

    for (let i = 0; i < tasksArray.length; i++) {
        ul.insertAdjacentHTML('afterbegin', getTemplate(tasksArray[i]));
    }
}

function clearList() {
    ul.innerHTML = '';
}

function generateTask(object) {
    ul.insertAdjacentHTML('afterbegin', getTemplate(object));
}

function editItem(item) {
    let parent = item.closest('li');
    let contentId = parent.dataset['id'];
    let content = parent.querySelector('.task-name');

    if (item.classList.contains('fa-save')) {
        content.setAttribute('contenteditable', 'true');
        content.focus();
    } else {
        content.setAttribute('contenteditable', 'false');
        content.blur();

        ajax.send({
            method: 'PATCH',
            url: `https://jsonplaceholder.typicode.com/todos/${contentId}`,
            data: JSON.stringify({
                title: content.textContent,
                completed: parent.classList.contains('bg-success')
            }),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            },
            success: function () {
                showMessage(updateAlert);
            },
            error: function (err) {
                showMessage(warningAlert);
                console.log(err);
            }
        });
    }
}

function deleteItem(item) {
    let contentId = item.closest('li').dataset['id'];

    ajax.send({
        method: 'DELETE',
        url: `https://jsonplaceholder.typicode.com/todos/${contentId}`,
        success: function () {
            showMessage(removeAlert);
        },
        error: function (err) {
            showMessage(warningAlert);
            console.log(err);
        }
    })
}

function showMessage(alert) {
    alert.classList.add('d-block');

    setTimeout(function () {
        alert.classList.remove('d-block');
    }, 2000);
}

function checkEmpty(list) {
    infoAlert.style.marginBottom = '0px';
    list.length > 0 ? infoAlert.classList.add('d-none') : infoAlert.classList.remove('d-none');
}


form.addEventListener('submit', function (e) {
    e.preventDefault();

    if (!inputText.value) {
        inputText.classList.add('is-invalid');
    } else {
        let data = {
            title: inputText.value,
            completed: false
        };

        ajax.send({
            method: 'POST',
            url: 'https://jsonplaceholder.typicode.com/todos',
            data: JSON.stringify(data),
            headers: {
                "Content-type": "application/json; charset=UTF-8"
            },
            success: function (res) {
                let response = JSON.parse(res);
                generateTask(response);
                showMessage(successAlert);
            },
            error: function (err) {
                showMessage(warningAlert);
                console.log(err)
            }
        });

        inputText.classList.remove('is-invalid');
        form.reset();
        checkEmpty(tasks);
    }
});

ul.addEventListener('click', function (e) {
    let parent = e.target.closest('li');

    if (e.target.classList.contains('delete-item')) {
        showMessage(removeAlert);
        deleteItem(e.target);
        parent.remove();
        checkEmpty(tasks);
    } else if (e.target.classList.contains('edit-item')) {
        e.target.classList.toggle('fa-save');
        editItem(e.target);
    } else if (e.target.classList.contains('status')) {
        e.target.classList.toggle('fa-times');
        e.target.classList.toggle('fa-check');
        parent.classList.toggle('bg-success');
        editItem(e.target);
    }
});

inputText.addEventListener('keyup', function () {
    if (inputText.value) {
        inputText.classList.remove('is-invalid');
    }
});
